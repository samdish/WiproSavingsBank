import java.io.*;
import java.util.*;
class WiproSavingsBank {
    
    String name;
    int age;
    String address;
    double avail_bal;
    static double min_bal;
    
    WiproSavingsBank(String n, int a, String str)
    {
        name = n;
        age = a;
        address = str;
        avail_bal = 0;
        min_bal = 1000.00;
        
    }
    
    void deposit(double amount)
    {
        avail_bal += amount;
        System.out.println("Amount deposited");
    }
    
    void withdrawal(double amount)
    {
        if(amount<=avail_bal-min_bal)
        {
            avail_bal -= amount;
            System.out.println("Amount withdrawn");
            
        }
        else if(amount>avail_bal-min_bal)
        System.out.println("Amount greater than available balance");
        
        
    }
    
	void balEnquiry()
	{
	    System.out.print("The available balance is: ");
	    System.out.println(avail_bal);
	}
	
	void ministatement()
	{
	    System.out.println("Mini Statement:");
	    System.out.print("Customer name: ");
	    System.out.println(name);
	    System.out.print("Customer age: ");
	    System.out.println(age);
	    System.out.print("Customer address: ");
	    System.out.println(address);
	    System.out.print("Available balance: ");
	    System.out.println(avail_bal);
	    
	}

    
	public static void main (String[] args) {
		
		String name1 = "Samdish Arora";
		int age1 = 22;
		String address1 = "Delhi";
		
		String name2 = "Zaheer Khan";
		int age2 = 40;
		String address2 = "Mumbai";
		
		WiproSavingsBank obj1 = new WiproSavingsBank(name1,age1,address1);
		WiproSavingsBank obj2 = new WiproSavingsBank(name2,age2,address2);
		
		obj1.deposit(5000);
		obj1.balEnquiry();
		
		obj1.deposit(2400.43);
		obj1.balEnquiry();
		
		obj1.withdrawal(5231.83);
		obj1.balEnquiry();
		obj1.ministatement();
		
		obj2.deposit(35000.56);
		obj2.balEnquiry();
		
		obj2.deposit(12400.43);
	    obj2.balEnquiry();
		
		obj2.withdrawal(55231.83);
		obj2.balEnquiry();
		
		obj2.withdrawal(34156.83);
		obj2.balEnquiry();
		obj2.ministatement();
		
		
	}
}